//
//  AppDelegate.swift
//  GlobalBase
//
//  Created by Alejandro  Rodriguez on 11/4/19.
//  Copyright © 2019 Alejandro Rodriguez. All rights reserved.
//

import UIKit
import PulentGlobal
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = UINavigationController.init(rootViewController: FinderFactory().getViewController())
        self.window?.makeKeyAndVisible()
        return true
    }
}

